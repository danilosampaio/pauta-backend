var Sequelize = require('sequelize'),
    epilogue = require('epilogue'),
    http = require('http');

// Define your models
var database = new Sequelize('pauta', 'postgres', 'postgres', {
  host: 'localhost',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

var User = database.define('User', {
  username: Sequelize.STRING,
  birthday: Sequelize.DATE
});

// Initialize server
var express = require('express'),
    bodyParser = require('body-parser'),
    passport = require('./auth/auth-local'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt'),
    authenticate = expressJwt({secret : 'server secret'});


var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Initialize epilogue
epilogue.initialize({
  app: app,
  sequelize: database
});

// Create REST resource
var userResource = epilogue.resource({
  model: User,
  endpoints: ['/users', '/users/:id']
});

////////
function serialize(req, res, next){
  /*create user*/
  req.user = {
    id: 'admin'
  };

  next();
};
function generateToken(req, res, next) {  
  req.token = jwt.sign({
    id: req.user.id
  }, 'server secret', {
    expiresIn: 120
  });
  next();
}
function respond(req, res) {  
  res.status(200).json({
    user: req.user,
    token: req.token
  });
}
app.use(passport.initialize());  
app.post('/auth', passport.authenticate(  
  'local', {
    session: false
  }), serialize, generateToken, respond);
/////////  

app.get('/modules', authenticate, function(req, res){
  res.send([{
      header: 'Projetos',
      subitems: [
        {
          name: "dashboard",
          label: "Dashboard"
        },
        {
          name: "gantt",
          label: "Cronograma - Gantt"
        },
        {
          name: "multaDiaria",
          label: "Multa diária"
        },
        {
          name: "ape",
          label: "Atraso por Entrega - APE"
        },
        {
          name: "pmas",
          label: "Prazo Máximo para Aceite do Serviço - PMAS"
        },
        {
          name: "pmcos",
          label: "Prazo Máximo para Conclusão da Ordem de Serviço - PMCOS"
        }
      ]
    },
    {
      header: 'Suporte Operacional',
      subitems: [
        {
          name: "incidentes",
          label: "Incidentes"
        },
        {
          name: "atividades",
          label: "Atividades"
        }
      ]
    },
    {
      header: 'Contrato BNB',
      subitems: [
        {
          name: "accrual",
          label: "Accrual"
        },
        {
          name: "pa",
          label: "Processos Administrativos"
        },
        {
          name: "baseDeConhecimento",
          label: "Base de Conhecimento"
        },
        {
          name: "time",
          label: "Time"
        }
      ]
    }
  ])
});

// Create database and listen
database
  .sync({ force: true })
  .then(function() {
    app.listen(8080, function(){
      console.log('listening at 8080');
    });
  });